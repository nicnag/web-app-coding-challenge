import React from 'react';

const NavbarHouses: React.FC<{}> = () => {
  return (
    <nav className="navbar navbar-dark bg-dark text-center">
      <span className="nav-text navbar-text">
        anapioficeandfire.com API Houses Overview
      </span>
    </nav>
  );
};

export default NavbarHouses;

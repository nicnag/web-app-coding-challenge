/* eslint-disable react/jsx-curly-newline */
import React from 'react';
import {
  getNumberFromLink,
  createPagination,
} from '../../services/editStringService';
import { NavbarHousesProps } from '../../types/NavbarHousesProps';

const PaginationHouses: React.FC<NavbarHousesProps> = ({
  linksString,
  active,
  getLinkDataFromChild,
}) => {
  const activePageIndex = getNumberFromLink(active);
  const paginationLinks = createPagination(linksString);
  const pageLinkObject = [];
  const lastPageIndex = getNumberFromLink(paginationLinks.last);
  if (paginationLinks.prev != null) {
    pageLinkObject.push({
      index: Number(getNumberFromLink(paginationLinks.prev)),
      link: paginationLinks.prev,
    });
  }
  pageLinkObject.push({
    index: Number(activePageIndex),
    link: active,
  });
  if (paginationLinks.next != null) {
    pageLinkObject.push({
      index: Number(getNumberFromLink(paginationLinks.next)),
      link: paginationLinks.next,
    });
  }
  return (
    <nav aria-label="housesPagination" className="navbar-dark bg-dark">
      <ul className="pagination justify-content-center">
        <li
          key="prev"
          className={
            Number(activePageIndex) === 1 ? 'disabled page-item' : 'page-item'
          }
        >
          <button
            type="button"
            className="page-link"
            tabIndex={-1}
            onClick={() => getLinkDataFromChild(paginationLinks.prev)}
          >
            Previous
          </button>
        </li>
        {pageLinkObject.map((pageLink, i) => (
          <li
            key={i.toString()}
            className={
              Number(activePageIndex) === Number(pageLink.index)
                ? 'page-item active'
                : 'page-item'
            }
          >
            <button
              type="button"
              onClick={() => getLinkDataFromChild(pageLink.link)}
              className="page-link"
            >
              {pageLink.index}
              <span className="sr-only">(current)</span>
            </button>
          </li>
        ))}
        <li
          key="next"
          className={
            Number(activePageIndex) === Number(lastPageIndex)
              ? 'disabled page-item'
              : 'page-item'
          }
        >
          <button
            type="button"
            className="page-link"
            onClick={() => getLinkDataFromChild(paginationLinks.next)}
          >
            Next
          </button>
        </li>
      </ul>
    </nav>
  );
};

export default PaginationHouses;

import React from 'react';
import useHouseService from '../../services/useHouseService';
import PaginationHouses from './PaginationHouses';
import Loading from '../status/Loading';
import Error from '../status/Error';
import House from '../House/House';
import NavbarHouses from './NavbarHouses';

export interface HouseProps {
  passLinkData: (link: string) => void;
  activeLink: string;
}

const Houses: React.FC<{}> = () => {
  const [activePaginationLink, setActivePaginationLink] = React.useState(
    'https://anapioficeandfire.com/api/houses?page=1&pageSize=15'
  );
  const service = useHouseService(activePaginationLink);
  const [url, setUrl] = React.useState('');
  const close = () => {
    setUrl('');
  };
  const reRenderHouses = (activeLink: string) => {
    setActivePaginationLink(activeLink);
  };
  if (service.status === 'loaded') {
    // eslint-disable-next-line array-callback-return
    service.payload.houses.map((house, i) => {
      const replace = house.url.replace(/\D/g, '');
      service.payload.houses[i].index = Number(replace);
    });
  }
  return (
    <div className="dark container h-100">
      {service.status === 'loading' && <Loading />}
      {url === '' && service.status === 'loaded' && <NavbarHouses />}
      {url === '' && service.status === 'loaded' && (
        <table className="table table-dark table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Region</th>
            </tr>
          </thead>
          <tbody>
            {service.payload.houses.map(house => (
              <tr
                key={house.url}
                onClick={() => {
                  setUrl(house.url);
                }}
              >
                <th scope="row">{house.index}</th>
                <td>{house.name}</td>
                <td>{house.region}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
      {service.status === 'loaded' && url === '' && (
        <PaginationHouses
          linksString={service.payload.link}
          active={activePaginationLink}
          getLinkDataFromChild={reRenderHouses}
        />
      )}
      {!!url && <House url={url} passOnClose={close} />}
      {service.status === 'error' && <Error />}
    </div>
  );
};

export default Houses;

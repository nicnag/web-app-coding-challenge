import React from 'react';
import backendError from '../../assets/backendError.png';

const Error: React.FC<{}> = () => {
  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-6">
          <img src={backendError} className="img-fluid" alt="backend_error" />
        </div>
        <div className="text-light">
          <h2> Error, the backend moved to the dark side.</h2>
        </div>
      </div>
    </div>
  );
};

export default Error;

import React from 'react';

export interface NavProps {
  onClose: () => void;
  name: string;
}

const NavbarHouse: React.FC<NavProps> = ({ onClose, name }) => {
  return (
    <nav className="navbar navbar-dark bg-dark">
      <form className="form-inline">
        <button className="btn btn-dark" type="button" onClick={onClose}>
          &#8249;
        </button>
      </form>
      <span className="text-center navbar-text">{name}</span>
    </nav>
  );
};

export default NavbarHouse;

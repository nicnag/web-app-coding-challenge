import React from 'react';
import useHouseByUrlService from '../../services/useHouseByUrlService';
import Loading from '../status/Loading';
import Error from '../status/Error';
import { HouseProps } from '../../types/HouseProps';
import NavbarHouse from './NavbarHouse';

const House: React.FC<HouseProps> = ({ url, passOnClose }) => {
  const service = useHouseByUrlService(url);
  return (
    <div className="container h-100">
      {service.status === 'loaded' && (
        <NavbarHouse name={service.payload.name} onClose={passOnClose} />
      )}
      {service.status === 'loading' && <Loading />}
      {service.status === 'loaded' && (
        <table className="table table-dark">
          <tbody>
            {Object.entries(service.payload).map(entries => (
              <tr>
                <th scope="row">{entries[0]}</th>
                {Array.isArray(entries[1]) && entries[1].length > 1 ? (
                  <td>
                    <table>
                      <tbody>
                        {entries[1].map((subentries: string) =>
                          subentries.length > 0 ? (
                            <tr>
                              <td>{subentries}</td>
                            </tr>
                          ) : (
                            ''
                          )
                        )}
                      </tbody>
                    </table>
                  </td>
                ) : (
                  <td>{entries[1]}</td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      )}
      {service.status === 'error' && <Error />}
    </div>
  );
};

export default House;

export interface HouseProps {
  url: string;
  passOnClose: () => void;
}

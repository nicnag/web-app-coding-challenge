export interface NavbarHousesProps {
  linksString: string | null;
  active: string;
  getLinkDataFromChild: (link: string) => void;
}

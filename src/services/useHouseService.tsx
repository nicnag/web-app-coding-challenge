import { useEffect, useState } from 'react';
import { Service } from '../types/Service';
import { House } from '../types/House';

export interface Result {
  link: string | null;
  houses: House[];
}

const useHouseService = (url: string) => {
  const [result, setResult] = useState<Service<Result>>({
    status: 'loading',
  });

  useEffect(() => {
    if (url) {
      setResult({ status: 'loading' });
      fetch(url)
        .then(response =>
          response.json().then(houses => ({
            link: response.headers.get('link'),
            houses,
          }))
        )
        .then(response => setResult({ status: 'loaded', payload: response }))
        .catch(error => setResult({ status: 'error', error }));
    }
  }, [url]);
  return result;
};

export default useHouseService;

// const res = response;
// const headers = res.headers.get('link');
/* async function fetchAPI() {
    let config = await fetch(url);
    
    if(config.ok) {
      let json = config.json();
      let header = config.headers.get('link');
      setResult({status: 'loaded', payload: json}):
    } else {

    }
  } */

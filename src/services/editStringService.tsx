export const getNumberFromLink = (link: string) => {
  const index = link.substring(
    link.lastIndexOf('page=') + 5,
    link.lastIndexOf('&')
  );
  return index;
};

export interface LooseObject {
  [key: string]: string;
}

export const getKey = (link: string) => {
  const response = link.substring(
    link.lastIndexOf('rel="') + 5,
    link.lastIndexOf('"')
  );
  return response;
};

export const getIndex = (url: string) => {
  const response = url.substring(
    url.lastIndexOf('houses/') + 7,
    url.lastIndexOf('"')
  );
  return response;
};

export const getSeperatedLink = (str: string) => {
  const response = str.match(/\bhttps?:\/\/\S+/gi);
  return response;
};

export const createPagination = (pagination: string | null) => {
  const paginationLinks: LooseObject = {};
  if (typeof pagination === 'string') {
    const seperatedLinks = pagination.match(/([^,]+)/g);
    if (seperatedLinks != null) {
      // eslint-disable-next-line array-callback-return
      seperatedLinks.map(seperatedLink => {
        const key = getKey(seperatedLink);
        const replace = seperatedLink.replace(/<|>/gi, ' ');
        const value = getSeperatedLink(replace);
        if (value != null) {
          const [destructedArray] = value;
          paginationLinks[key] = destructedArray;
        }
      });
    }
  }
  return paginationLinks;
};

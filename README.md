# Web-App Conding Challenge

## To run:
at the root `yarn` to install dependencies, `yarn dev` to run an application at http://localhost:3000 and `yarn run build` to pack the production website.

## Framework: 
- React

## Bundler:
- Webpack

## Other:
- Bootstrap
